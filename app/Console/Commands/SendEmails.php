<?php

namespace App\Console\Commands;

use App\Email;
use Illuminate\Console\Command;
use Mail;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'phish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send phishing emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        date_default_timezone_set('America/New_York');
        $emails = Email::all();
        $i = 0;
        foreach ($emails as $email) {
            //echo "$email->email\n";
            Mail::send('email', [], function ($m) use ($email) {
                $m->from('oit@myutk.utk.edu', 'OIT HelpDesk');
                $m->to($email->email)->subject("Password expires for your NetID on ".date('m/d/Y')." at 11:59:59 PM");
            });
            $i++;
        }
        echo "$i emails were successfully sent.\n";
    }
}
