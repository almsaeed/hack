<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>You have been caught!</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div style="max-width: 750px;margin: 30px auto;">
                <h1>You have been caught!</h1>

                <p class="lead">
                    This is a simulation of a phishing website. The intent is to trick a UTK student into exposing
                    his/her NetID and password.
                </p>
                <p class="lead">
                    The following list of NetIDs and passwords have been caught via this site (some data are
                    automatically generated as mock test data).
                </p>
                <table class="table table-striped">
                    <tr>
                        <th>NetID</th>
                        <th>Password</th>
                    </tr>
                    @foreach($records as $record)
                        <tr>
                            <td>{{$record->netid}}</td>
                            <td>{{$record->password}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>