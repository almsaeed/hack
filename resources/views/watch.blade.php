<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>You have been caught!</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <video controls>
                <source src="/include/test2.mp4" type="video/mp4">
                Your browser doesn't support MP4 video
            </video>
        </div>
    </div>
</div>
</body>
</html>