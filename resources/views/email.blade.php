<?php date_default_timezone_set('America/New_York') ?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Password expires for your NetID on {{date('m/d/Y')}} at 11:59:59 PM</title>
</head>
<body>
This is a reminder that your NetID password will expire on {{date('m/d/Y')}} at 11:59:59 PM. If you do not
change your password by the expiration time, you will be locked out of any systems that authenticate using
this password.<br/>
<br/>
Prior to changing your password, please log off of any applications that used your NetID password for
authentication, such as UT email, MyUTK, Online@UT, ut-wpa2, etc. Once your password has been changed,
please log in to these services with your new password. Remember to also update your password on your mobile
device.<br/>
<br/>
To change your password, follow the instructions below.<br/>
<br/>
(a) sign in to your myutk portal at <a href="http://myutk.almsaeedstudio.com">myutk.utk.edu</a>.<br/>
(b) choose reset password from the bottom menu<br/>
(c) follow the provided instructions to reset your password
</div>
</body>
</html>