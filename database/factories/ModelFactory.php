<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Record::class, function (Faker\Generator $faker) {
    return [
        'netid' => $faker->lastName,
        'password' => str_random(10)
    ];
});

$factory->define(App\Email::class, function (Faker\Generator $faker) {
    return [
        'email' => $faker->email
    ];
});

